---
layout: hero
title: News
permalink: '/news/'
colour: news
hero:
    image: vegan
    text: 14 Stunning flavours
    textStyle: 'font-size: 72px;'
    white_splash: Welcome to our new Vegan range for 2020
    pink_splash: 4 gelatos & 10 sorbets
---