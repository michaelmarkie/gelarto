!function(){

    document.addEventListener('DOMContentLoaded', function() {

        if(document.getElementById('tab-content-uk'))
            fetch(site.baseurl + '/assets/locations.json')
                .then(resp => resp.json())
                .then( data => {

                    var uk_tab = document.getElementById('tab-content-uk');
                    uk_tab.innerHTML = '';

                    var uk_template = Handlebars.compile(document.querySelector('#uk-tmpl').innerHTML);

                    data.uk.each( v => uk_tab.innerHTML += uk_template(v) );

                })
                .then(function () {
                    refresh_maps();
                });

    });

    var refresh_maps = function () {

        document.querySelectorAll('.map').each( function () {

            var map = L.map(this).setView(this.getAttribute('data-latlng').split(','), 13);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                maxZoom: 20,
                attribution: 'Imagery &copy; <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoibWljaGFlbG1hcmtpZSIsImEiOiJjazB0Ym03MnQwNmd5M25waHFncjNndDB3In0.4lkEZ7nX07NEvFTpZpqVGA'
            }).addTo(map);

            L.marker(this.getAttribute('data-latlng').split(',')).addTo(map);

        });
    };

}();