var next,prev,goto,lock;
(function() {
    var n = 1, // Track which is active banner
        s = 8e3, // How many ms till timeout
        r, // setTimeout variable so we can clear it
        b = document.querySelector('.banners'),
        i = b?b.querySelectorAll('.banner').length:0,
        c = number => {
            // We want to clear it incase someone manually triggered next/prev function.
            r = clearTimeout(r);

            // Remove active across the board
            b.querySelector('.active').classList.remove('active');
            // Find the child and activate it.
            b.querySelector('.banner:nth-child(' + number + ')').classList.add('active');

            if ( document.querySelector('.dots') ) {
                document.querySelector('.dot.active').classList.remove('active');
                document.querySelector('.dot:nth-child(' + number + ')').classList.add('active');
            }

            r = setTimeout(next, s);
        };
    next = () => {
        // if banner number = total banners reset to 1 else add 1
        c(n === i?n = 1:n += 1);
    };
    prev = () => {
        // if banner number = starting number (1) set to last banner else subtract 1
        c(n === 1?n = i:n -= 1);
    };
    goto = c;
    lock = num => {
        c(num);
        clearTimeout(r);
    };

    if ( b )
        r = setTimeout(next, s);
})();