---
layout: post
title: Vegan
categories: [news]
image: vegan.jpg
---
Gelarto offers a comprehensive range of traditional and new flavours in a variety of sizes from 5-litres to individual portions.

Our pricing is uncomplicated with a simple price point and without the complicated multi-tiered pricing favoured by many ice-cream companies.

Our range is Kosher and Vegan certified and we offer gluten-free, dairy-free and fat-free flavours so there is something for everyone